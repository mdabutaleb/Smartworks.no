<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'd302778');

/** MySQL database username */
define('DB_USER', 'd302778');

/** MySQL database password */
define('DB_PASSWORD', '6328fhda');

/** MySQL hostname */
define('DB_HOST', 'mysql04.fastname.no');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '0rHhLIlZ2BOpKFlJTqSnC2fF6mXgZ69w2fF7FInReLdFXIQBU2r3mTibjsyMsk88');
define('SECURE_AUTH_KEY',  't07j7jfx8gjB8KVTWab7iOR99Ps8sgMbvduCrCVuu565mmz65y6igQ6K1fbnjrZp');
define('LOGGED_IN_KEY',    'wal6mE6s5RhTKLz8Jnhf7gWjsaA1idUy1eJbq6lruynAFvbBxe4LI7e8vJDDwtn4');
define('NONCE_KEY',        'y50qcQutHRCvYQZrH1bZrvC8Wtez2H8kOEKCcXboYEpuR9E6ZXmrjetQl4hyb9H8');
define('AUTH_SALT',        'LiWzWIyImPgcI01PfHXr4QWW3WGWKSxVYxurLm6GaKxy0pHPzTHCryVN68tktq9d');
define('SECURE_AUTH_SALT', 'P5HpWLnnDMaxyMWJiG1Pn3ZThHwUjcGzvdwqRQ4F9FkOH1tCU1KRTpqVJj5hIgPK');
define('LOGGED_IN_SALT',   'zSMnrSi6hwjXtccoDLeJbW4v1VMtvxIkUBIbyuSb2Qao9JrTzCu6s6dwmF79DlVq');
define('NONCE_SALT',       'WrgpmF4DAzda3DQ6g3Bt8pIXoywDnZyGs9RjnxpGrqqfsB3Bbaau04EGogy7PSYY');

/**
 * Other customizations.
 */
define('FS_METHOD','direct');define('FS_CHMOD_DIR',0755);define('FS_CHMOD_FILE',0644);
define('WP_TEMP_DIR',dirname(__FILE__).'/wp-content/uploads');

/**
 * Turn off automatic updates since these are managed upstream.
 */
define('AUTOMATIC_UPDATER_DISABLED', true);


/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'db_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
